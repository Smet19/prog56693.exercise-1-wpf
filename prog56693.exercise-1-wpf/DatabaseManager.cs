﻿using System.Linq;
using System.Collections.Generic;
using System.Data;
using Microsoft.Data.Sqlite;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Driver;
using System.Windows;

namespace prog56693.exercise_1_wpf
{
    public static class DatabaseManager
    {
        // Enum-like functionality
        public class TableName
        {
            private TableName(string _value) { Value = _value; IsRound = false; }
            private TableName(string _value, bool _isRound) { Value = _value; IsRound = _isRound; }
            public string Value { get; private set; }
            public bool IsRound { get; private set; }

            public static TableName Round1 { get { return new TableName("Round1", true); } }
            public static TableName Round2 { get { return new TableName("Round2", true); } }
            public static TableName Round3 { get { return new TableName("Round3", true); } }
            public static TableName TotalShots { get { return new TableName("TotalShots"); } }
            public static TableName DeathCoord { get { return new TableName("DeathCoord"); } }

        }

        public class Highscore
        {
            [BsonId]
            public int id { get; set; }
            public string Username { get; set; }
            public float Accuracy { get; set; }
            public int TotalRoundsWon { get; set; }
        }

        public class RoundData
        {
            public float avPLSh { get; set; }
            public float avAISh { get; set; }
            public float avTime { get; set; }
            public float avPLDst { get; set; }
            public float avAIDst { get; set; }
        }

        public class ShotsFiredTotal
        {
            public int Player { get; set; }
            public int AI { get; set; }
        }

        public static class MongoDB
        {
            private static string connectionString = "mongodb+srv://GDAP-Student:jous.trok4POOS_pood@" +
                                                    "gdap2020-cluster.swxsa.mongodb.net/" +
                                                    "GDAP_Exercise?retryWrites=true&w=majority";
            public static List<Highscore> GetHighScores()
            {
                List<Highscore> highscores = new List<Highscore>();
                MongoClient client = new MongoClient(connectionString);
                var db = client.GetDatabase("GDAP_Exercise");

                highscores = db.GetCollection<Highscore>("Highscores").Find(_ => true)
                            .Sort(Builders<Highscore>.Sort.Descending(a => a.TotalRoundsWon))
                            .ToList();
                return highscores;
            }
        }

        public static class SQLite
        {
            private static string connectionString = "Data Source=";

            public static void UpdateConnString()
            {
                connectionString = "Data Source=" + (App.Current.MainWindow as MainWindow).FilePathText.Text;
            }

            // Reads largest MatchID from Round1 table, returns incremented value
            public static ShotsFiredTotal GetShotsFiredTotal()
            {
                ShotsFiredTotal shotsFired = new ShotsFiredTotal();

                using (IDbConnection connection = new SqliteConnection(connectionString))
                {
                    connection.Open();
                    IDbCommand dbcmd = connection.CreateCommand();
                    dbcmd.CommandText = "SELECT PLShots, AIShots FROM TotalShots LIMIT 1";

                    IDataReader reader = dbcmd.ExecuteReader();
                    while (reader.Read())
                    {
                        shotsFired.Player = reader.GetInt32(0);
                        shotsFired.AI = reader.GetInt32(1);
                    }

                    reader.Close();
                    dbcmd.Dispose();
                    connection.Close();
                }
                return shotsFired;
            }

            public static RoundData GetRoundAverage(TableName _round)
            {
                if (!_round.IsRound)
                {
                    throw new System.ArgumentException($"{_round.Value} is not a Round table");
                }

                RoundData ret = new RoundData();

                using (IDbConnection connection = new SqliteConnection(connectionString))
                {
                    connection.Open();
                    IDbCommand dbcmd = connection.CreateCommand();
                    dbcmd.CommandText = $"SELECT avg(PLShots), avg(AIShots), avg(Time), avg(PLDist), avg(AIDist) FROM {_round.Value}";

                    IDataReader reader = dbcmd.ExecuteReader();
                    while (reader.Read())
                    {
                        ret.avPLSh = reader.GetFloat(0);
                        ret.avAISh = reader.GetFloat(1);
                        ret.avTime = reader.GetFloat(2);
                        ret.avPLDst = reader.GetFloat(3);
                        ret.avAIDst = reader.GetFloat(4);
                    }

                    reader.Close();
                    dbcmd.Dispose();
                    connection.Close();
                }
                return ret;
            }

            public static List<Point> GetDeathPoints()
            {
                List<Point> points = new List<Point>();

                using (IDbConnection connection = new SqliteConnection(connectionString))
                {
                    connection.Open();
                    IDbCommand dbcmd = connection.CreateCommand();
                    dbcmd.CommandText = $"SELECT X, Z FROM DeathCoord";

                    IDataReader reader = dbcmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Point point = new Point();
                        point.X = reader.GetFloat(0);
                        point.Y = reader.GetFloat(1);
                        points.Add(point);
                    }

                    reader.Close();
                    dbcmd.Dispose();
                    connection.Close();
                }
                return points;
            }
        }
    }
}

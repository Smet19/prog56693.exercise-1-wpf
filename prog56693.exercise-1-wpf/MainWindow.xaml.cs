﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using Microsoft.Win32;

namespace prog56693.exercise_1_wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        WriteableBitmap writeableBitmap;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void UpdateBitmap()
        {
            BitmapSource source = (BitmapSource)MapImage.Source;
            writeableBitmap = new WriteableBitmap(source);
            MapImage.Source = writeableBitmap;

            // map is 110 by 110, 55 is max distance from 0
            double x_factor = writeableBitmap.Width / 110;
            double y_factor = writeableBitmap.Height / 110;

            List<Point> points = DatabaseManager.SQLite.GetDeathPoints();
            foreach(Point point in points)
            {
                int row = Convert.ToInt32((-point.Y + 55) * y_factor);
                int column = Convert.ToInt32((point.X + 55) * x_factor);

                if (column < 0 || row < 0 ||
                    column >= writeableBitmap.PixelWidth || row >= writeableBitmap.PixelHeight)
                {
                    return;
                }
                try
                {
                    writeableBitmap.Lock();
                    unsafe
                    {
                        IntPtr pBackBuffer = writeableBitmap.BackBuffer;

                        pBackBuffer += row * writeableBitmap.BackBufferStride;
                        pBackBuffer += column * 4;

                        int colour = 255 << 16; // R
                        colour |= 0 << 8;       // G
                        colour |= 0 << 0;       // B

                        *((int*)pBackBuffer) = colour;
                    }
                    writeableBitmap.AddDirtyRect(new Int32Rect(column, row, 5, 5));
                }
                finally
                {
                    writeableBitmap.Unlock();
                }
            }
        }
        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
        }

        private void FilePathButton_Click(object sender, RoutedEventArgs e)
        {
            string prevFilePath = FilePathText.Text;

            OpenFileDialog fd = new OpenFileDialog();
            fd.Filter = "DB files (*.db)|*.db";
            if (fd.ShowDialog() == true && prevFilePath != fd.FileName)
            {
                FilePathText.Text = fd.FileName;

                DatabaseManager.SQLite.UpdateConnString();

                DatabaseManager.ShotsFiredTotal shotsFired = DatabaseManager.SQLite.GetShotsFiredTotal();

                PLShots_TB.Text = shotsFired.Player.ToString();
                AIShots_TB.Text = shotsFired.AI.ToString();

                List<DatabaseManager.Highscore> highscores = DatabaseManager.MongoDB.GetHighScores();
                ListView_Data.ItemsSource = highscores;
                UpdateBitmap();
            }
        }

        private void Rounds_CMB_Loaded(object sender, RoutedEventArgs e)
        {
            List<DatabaseManager.TableName> rounds = new List<DatabaseManager.TableName>();

            rounds.Add(DatabaseManager.TableName.Round1);
            rounds.Add(DatabaseManager.TableName.Round2);
            rounds.Add(DatabaseManager.TableName.Round3);

            Rounds_CMB.ItemsSource = rounds;
        }

        private void Rounds_CMB_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            DatabaseManager.TableName selectedRound = Rounds_CMB.SelectedItem as DatabaseManager.TableName;
            DatabaseManager.RoundData roundData = DatabaseManager.SQLite.GetRoundAverage(selectedRound);

            AvgSF_PL_TB.Text = roundData.avPLSh.ToString();
            AvgSF_AI_TB.Text = roundData.avAISh.ToString();
            AvgTOR_TB.Text = roundData.avTime.ToString();
            AvgDM_PL.Text = roundData.avPLDst.ToString();
            AvgDM_AI.Text = roundData.avAIDst.ToString();
        }
    }
}
